FROM php:7.3.4-cli-alpine
LABEL maintainer="David Kudera <kudera.d@gmail.com>"

ARG PHP_METRICS_VERSION

ENV COMPOSER_VERSION="1.8.5"
ENV COMPOSER_ALLOW_SUPERUSER="1"

RUN apk add --no-cache curl && \
	curl -sL -o composer.phar https://getcomposer.org/download/${COMPOSER_VERSION}/composer.phar && \
	mv composer.phar /usr/local/bin/composer && \
	chmod +x /usr/local/bin/composer && \
	composer global require phpmetrics/phpmetrics:v${PHP_METRICS_VERSION} && \
	mkdir -p /app

WORKDIR /app
VOLUME /app
ENTRYPOINT ["/root/.composer/vendor/bin/phpmetrics"]
